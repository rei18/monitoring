<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
	<title>Monitoring Dokumen</title>
		<meta name="description" content="">
    	<meta name="viewport" content="width=device-width">
    <!-- Google Web Font Embed -->
    <link href="assets/css/style.css" rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css');?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-datetimepicker.min.css');?>">
    	<link rel="stylesheet" href="<?php echo base_url('assets/css/templatemo_main.css');?>">
</head>
<body>
	<div id="main-wrapper">
    <!-- Header -->
    <nav class="navbar navbar-default">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">PTDI | Monitoring Dokumen</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="<?php echo site_url('login');?>">Logout</a></li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
			<div class="container">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 templatemo-content-wrapper">
                    <div class="templatemo-content">
						<section>
                            <div class="row">
                                <div class="black-bg col-sm-12 col-md-12 col-lg-12">
                                    <h2 class="text-center">Data Dokumen Berhasil Direkap</h2>
                                    <div class="col-sm-12 col-md-12 margin-bottom-20">
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="row margin-top-20">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-bottom-20">
                                    <a href="<?php echo site_url('welcome');?>" class="change-section-login">
                                        <div class="black-bg btn-menu">
                                            <h2>Kembali Ke Menu</h2>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
    </div>
	<div id="preloader">
        <div id="status">&nbsp;</div>
    </div><!-- /#preloader -->

	<script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/jquery-ui.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.backstretch.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/templatemo_script.js');?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.js');?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-datetimepicker.min.js');?>"></script>
</body>
</html>