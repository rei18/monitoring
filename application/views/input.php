<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
	<title>PTDI - Monitoring Dokumen</title>
		<meta name="description" content="">
    	<meta name="viewport" content="width=device-width">
    <!-- Google Web Font Embed -->
    <link href="assets/css/style.css" rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css');?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-datetimepicker.min.css');?>">
    	<link rel="stylesheet" href="<?php echo base_url('assets/css/templatemo_main.css');?>">
</head>
<body>
	<div class="image-section">
        <div class="image-container">
            <img src="assets/images/bghome.jpg" id="menu-img" class="main-img inactive" alt="Zoom HTML5 Template by templatemo.com">
            <img src="assets/images/zoom-bg-5.jpg" id="input-img" class="inactive" alt="input">
        </div>
    </div>
    
    <!-- Header -->
    <nav class="navbar navbar-default">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">PTDI | Monitoring Dokumen</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="<?php echo site_url('login');?>">Logout</a></li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
    <div class="container">
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 templatemo-content-wrapper">
    	    <div class="templatemo-content">
                <section id="menu-section" class="active">
                    <div class="row">
                        <div class="black-bg col-sm-12 col-md-12 col-lg-12">
                            <h2 class="text-center">Data Dokumen</h2>
                            <div class="col-sm-12 col-md-12 margin-bottom-20">
                                <form action="<?php echo site_url('dokumen/aksi');?>" method="post" autocomplete="off">
                                    <div class="form-group">
                                    <!--<label for="input_name">Name</label>-->
                                        <input type="text" id="no_nota" name="no_nota" class="form-control" placeholder="No Nota">
                                    </div>
                                    <div class="text-center">
                                        <button class="btn btn-primary" type="submit" name="status" value="Masuk">Tambah</button>
                                    </div>
                                </form>
                            </div>
                        <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="row margin-top-20">
                        <div class="col-xs-6 col-sm-12 col-md-12 col-lg-12">
                            <a href="<?php echo site_url('welcome');?>" class="change-section-login">
                                <div class="black-bg btn-menu">
                                    <h2>Batal</h2>
                                </div>
                            </a>
                        </div>
                    </div>
      	        </section>
            </div>
        </div>
	</div>
	<div id="preloader">
        <div id="status">&nbsp;</div>
    </div><!-- /#preloader -->

	<script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/jquery-ui.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.backstretch.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/templatemo_script.js');?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.js');?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-datetimepicker.min.js');?>"></script>

    <script type="text/javascript">
        $(function () {
            $('#tgl_masuk').datepicker({
                autoclose : true
            });
        });
    </script>
</body>
</html>