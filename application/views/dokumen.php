<!DOCTYPE html>
<html>
<head>
	<title>Pemesanan - Kawanan Rusa</title>

    <!-- Google Web Font Embed -->
    <link href="assets/css/style.css" rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css');?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-datetimepicker.min.css');?>">
</head>
<body>
    
    <!-- Header -->
    <nav class="navbar navbar-default">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">PTDI | Monitoring Dokumen</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="<?php echo site_url('welcome');?>" style="text-decoration: none;" >Menu</a></li>
            <li><a href="<?php echo site_url('login');?>">Logout</a></li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title">Data Dokumen</h3>
                  </div>
                  <div class="panel-body">
                  <!-- Form Pencarian -->
                    <div class="row">
                        <div class="col-sm-12 text-right">
                            <form action="" class="form-inline">
                                <div class="form-group">
                                    <select name="filter" id="filter" class="form-control">
                                        <option value="">Cari Berdasarkan</option>
                                        <option <?php if($filter == "no_nota"){ echo "selected";}?> value="no_nota">No Nota</option>
                                        <option <?php if($filter == "tgl_masuk"){ echo "selected";}?> value="tgl_masuk">Tanggal</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="pencarian" placeholder="Masukan Data Pencarian" value="<?php echo $pencarian;?>">
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-success">Cari</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- Table List Pemesanan -->
                    <div>
                    <table class="table table-striped table-bordered" style="margin-top: 20px;">
                        <thead>
                            <th><center>No.</th>
                            <th><center>Nomor Nota</th>
                            <th><center>Tanggal Masuk</th>
                            <th><center>Jam Masuk</th>
                            <th><center>Batas Pengiriman</th>
                            <th><center>Keterangan</th>
                            <th><center>Action</th>
                            <th><center>Status</th>
                        </thead>
                        <tbody>
                            <?php if(sizeof($dokumen) > 0) : ;?>
                                <?php foreach ($dokumen as $key => $result) : ?>
                                    <?php $expired = date('Y-m-d', strtotime($result->tgl_masuk . ' +2 day')); ?>
                                    <tr>
                                        <td><center><?php echo $key + 1;?></td>
                                        <td><?php echo $result->no_nota;?></td>
                                        <td><center><?php echo date_format(date_create($result->tgl_masuk), 'd F Y');?></td>
                                        <td><center><?php echo date_format(date_create($result->tgl_masuk), 'H:i');?></td>
                                        <td><center><?php echo date_format(date_create($expired), 'd F Y'); ?></td>
                                        <td><center>
                                            <a href="<?php echo site_url('dokumen/edit/'. $result->id_dokumen);?>">Edit</a> |
                                            <a href="<?php echo site_url('dokumen/delete/'. $result->id_dokumen);?>" onclick="return confirm('Anda yakin akan menghapus data ini ?');">Delete</a>
                                        </td>
                                        <td>
                                            <div class="dropdown">
                                                <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown">Action
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#">Kirim</a></li>
                                                    <li><a href="#">Edit</a></li>
                                                    <li><a href="#">Delete</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td><center><?php echo $result->status;?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else : ?>
                                <tr>
                                    <td colspan="8" class="text-center">Data Dokumen Kosong</td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>

	<script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/jquery-ui.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.backstretch.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/templatemo_script.js');?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.js');?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-datetimepicker.min.js');?>"></script>
</body>
</html>