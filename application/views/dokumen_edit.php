<!DOCTYPE html>
<html>
<head>
	<title>Edit dokumen - Kawanan Rusa</title>

    <!-- Google Web Font Embed -->
    <link href="assets/css/style.css" rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css');?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-datetimepicker.min.css');?>">
</head>
<body>
    
    <!-- Header -->
    <nav class="navbar navbar-default">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">PTDI | Monitoring Dokumen</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="<?php echo site_url('dokumen/index_index');?>">Dokumen</a></li>
            <li><a href="<?php echo site_url('login');?>">Logout</a></li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title">Edit Data dokumen</h3>
                  </div>
                  <div class="panel-body">
                    <form action="<?php echo site_url('dokumen/update/'. $dokumen->id_dokumen);?>" method="post">
                        <div class="form-group">
                            <label for="no_nota">No Nota</label>
                            <input type="text" id="no_nota" name="no_nota" class="form-control" placeholder="No Nota" / value="<?php echo $dokumen->no_nota;?>">
                        </div>
                        <button class="btn btn-primary" name="status" value="Dikirim ke Divisi HD">Kirim</button>
                    </form>
                  </div>
                </div>
            </div>
        </div>
    </div>

	<script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/jquery-ui.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.backstretch.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/templatemo_script.js');?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.js');?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-datetimepicker.min.js');?>"></script>

    <script type="text/javascript">
        $(function () {
            $('#tgl_masuk').datepicker({
                autoclose : true
            });
        });
    </script>
</body>
</html>