<!DOCTYPE html>
<html>
<head>
	<title>Halaman Login</title>

    <!-- Google Web Font Embed -->
    <link href="assets/css/style.css" rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/templatemo_main.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/login.css');?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-datetimepicker.min.css');?>">
</head>
<body>
	<div class="row margin-top-50 login">
        <div class="black-bg col-sm-4 col-sm-offset-4">
            <h2 class="text-center">Admin</h2>
            <div class="col-sm-12 col-md-12 margin-bottom-20">
                <form action="<?php echo site_url('login/proses');?>"" method="post">
                    <div class="form-group">
                        <!--<label for="contact_name">Name</label>-->
                        <input type="text" id="username" name="username" class="form-control" placeholder="Username" />
                    </div>
                    <div class="form-group">
                        <!--<label for="contact_email">No Telpon</label>-->
                        <input type="password" id="password" name="password" class="form-control" placeholder="Password" />
                    </div>
                    <div class="text-right">
                   		<button class="btn btn-primary" name="login">Login</button>
                    </div>
                </form>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div  class="row margin-top-50 login">
        <div class="col-sm-4 col-sm-offset-4">
            <a href="#" class="change-section">
                <div class="black-bg btn-menu">
                    <h2>Buat Akun</h2>
                </div>
            </a>
        </div>
    </div>

	<script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery-ui.min.js"></script>
    <script src="assets/js/jquery.backstretch.min.js"></script>
    <script src="assets/js/templatemo_script.js"></script>
    <script src="assets/js/bootstrap.js"></script>
    <script src="assets/js/bootstrap-datetimepicker.min.js"></script>
</body>
</html>