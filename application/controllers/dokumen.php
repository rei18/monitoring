<?php

class Dokumen extends CI_Controller{

function __construct(){
parent::__construct();
$this->load->model('m_input');
$this->load->helper('url');

}

function index(){
$data['dokumen'] = $this->m_input->tampil_data()->result();
$this->load->view('v_rekap',$data);

}

function tambah(){
$this->load->view('welcome_message');
}

function aksi(){
$no_nota = $this->input->post('no_nota');
//$tgl_masuk = $this->input->post('tgl_masuk');
$status = $this->input->post('status');

$data = array(
'no_nota' => $no_nota,
'tgl_masuk' => date('Y-m-d G:i'),
'status' => $status,
);
$this->m_input->input_data($data,'dokumen');
redirect('dokumen/index');
}

function set_rules(){
	$rules['no_nota'] = 'trim|required';
	$rules['tgl_masuk'] = 'trim|required';
	$this->validation->set_rules($rules);
	$this->validation->set_message('required', '* harus diisi');
}

function index_table()
{
	$query 		= $this->db->get('dokumen')->result();

	$pencarian 	= $this->input->get('pencarian');
	$filter 	= $this->input->get('filter');

	//pencarian
	if (!empty($pencarian)) {
		$query = $this->db->like($filter, $pencarian, 'both')->get('dokumen')->result();
	}

	//array data
	$data['dokumen'] 	= $query;
	$data['pencarian'] 	= $pencarian;
	$data['filter'] 	= $filter;

	$this->load->view('dokumen', $data);
}

function edit($id)
{
	$query = $this->db->get_where('dokumen', ['id_dokumen' => $id])->row();
	
	//array data
	$data['dokumen'] 	= $query;

	$this->load->view('dokumen_edit', $data);
}

function update($id)
{
	$no_nota 		= $this->input->post('no_nota');
	//$tgl_masuk 	= $this->input->post('tgl_masuk');
	$status = $this->input->post('status');
		//data update
	$data 		= array(
		'no_nota' => $no_nota,
		//'tgl_masuk' => date('Y-m-d', strtotime($tgl_masuk)),
		'status' => $status,

	);

	//query update
	$this->db->where('id_dokumen', $id)->update('dokumen', $data);

	redirect(site_url('dokumen/index_table'));
}

function delete($id)
{
	//query delete
	$this->db->delete('dokumen', ['id_dokumen' => $id]);
	redirect(site_url('dokumen/index_table'));
}

public function index_coba(){
	$data['dokumen'] = $this->m_input->get_status();
	$this->load->view('status',$data);
}

public function input(){
	$this->load->view('input');
}

}