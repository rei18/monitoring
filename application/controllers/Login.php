<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		$this->load->view('login');
	}

	public function proses()
	{
		$username 	= $this->input->post('username');
		$password 	= $this->input->post('password');
		$level		= $this->input->post('level');

		$query 		= $this->db->get_where('user', ['username' => $username, 'password' => md5($password), 'level' => $level]);

		//cek ketersediaan
		if ($query->num_rows() > 0) {
			//session
			if ($query -> level == 'admin') {
				redirect(site_url('welcome'));
			}elseif ($level == 'usdm') {
				redirect(site_url('bagian'));
			}
		} else {
			redirect(site_url('login'));
		}
	}
}
